package com.lottie.search.controller;

import com.lottie.search.dto.response.AnimationSearchResponse;
import com.lottie.search.entity.Animation;
import com.lottie.search.service.AnimationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class AnimationController {

    private AnimationService animationService;

    public AnimationController( AnimationService  theAnimationService){
        this.animationService = theAnimationService;
    }


    @GetMapping("/animations")
    public ResponseEntity<Page<AnimationSearchResponse>> getAnimationList(
            @RequestParam(required = false, defaultValue = "") String text,
            @RequestParam(required = false, defaultValue = "0,1") int[] type,
            @RequestParam(required = false, defaultValue = "0,1") int[] category,
            @RequestParam(required = false, defaultValue = "") String color,
            @RequestParam(required = false, defaultValue = "0,1,2,3,4") int[] style,
            @RequestParam(required = false, defaultValue = "12") int pageSize,
            @RequestParam(required = false, defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "title") String sort
            ){



        Pageable pageable = PageRequest.of(pageNo,pageSize, Sort.by(sort).descending());



        return ResponseEntity.ok(this.animationService.findAll(text,type,category,color,style, pageable));

    }

    private Specification<Animation> createSearchSpecification(String text, String type, String category, String color, String style ){

        Specification<Animation> specification = Specification.where(null);


        if (!text.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("title"),"%" + text + "%"));
        }

        if (!type.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("type"),type));
        }

        if (!category.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("category"),category));
        }

        if (!color.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("color"),color));
        }

        if (!style.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("style"),style));
        }


        return specification;

    }









}
