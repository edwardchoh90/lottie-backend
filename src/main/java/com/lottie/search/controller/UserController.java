package com.lottie.search.controller;

import com.lottie.search.entity.User;
import com.lottie.search.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class UserController {

    private UserService userService;

    public UserController(UserService  theUserService){
        this.userService = theUserService;
    }


    @GetMapping("/users")
    public ResponseEntity<Page<User>> getAnimationList(
            @RequestParam(required = false, defaultValue = "") String text,
            @RequestParam(required = false, defaultValue = "") String location,
            @RequestParam(required = false, defaultValue = "") String isHire,
            @RequestParam(required = false, defaultValue = "12") int pageSize,
            @RequestParam(required = false, defaultValue = "0") int pageNo
            ){


        Pageable pageable = PageRequest.of(pageNo,pageSize);


        Specification<User> spec = createSearchSpecification(text,location,isHire);

        return ResponseEntity.ok(this.userService.findAll(spec, pageable));

    }

    private Specification<User> createSearchSpecification(String text, String location, String isHire ){

        Specification<User> specification = Specification.where(null);


        if (!text.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("name"),"%" + text + "%"));
        }

        if (!location.isBlank()){
            specification =  specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("animatorLocation"),"%" + location + "%"));
        }

        if (!isHire.isBlank()) {
            specification = specification.and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("isHire"), isHire));
        }

        return specification;

    }









}
