package com.lottie.search.dto.response;

import javax.persistence.*;

public class AnimationSearchResponse {

    private String animatorUrl;

    private String avatar;

    private String email;

    private String animatorName;

    private String title;

    private String jsonFilePath;

    private String color;

    private String description;

    private String previewUrl;

    private int status;

    private int style;

    private int type;

    private Boolean hasAep;

    private int downloadCount;

    private long lastUpdatedAt;

    public AnimationSearchResponse() {
    }

    public AnimationSearchResponse(String animatorUrl, String avatar, String email, String animatorName, String title, String jsonFilePath, String color, String description, String previewUrl, int status, int style, int type, Boolean hasAep, int downloadCount, long lastUpdatedAt) {
        this.animatorUrl = animatorUrl;
        this.avatar = avatar;
        this.email = email;
        this.animatorName = animatorName;
        this.title = title;
        this.jsonFilePath = jsonFilePath;
        this.color = color;
        this.description = description;
        this.previewUrl = previewUrl;
        this.status = status;
        this.style = style;
        this.type = type;
        this.hasAep = hasAep;
        this.downloadCount = downloadCount;
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public String getAnimatorUrl() {
        return animatorUrl;
    }

    public void setAnimatorUrl(String animatorUrl) {
        this.animatorUrl = animatorUrl;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAnimatorName() {
        return animatorName;
    }

    public void setAnimatorName(String animatorName) {
        this.animatorName = animatorName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJsonFilePath() {
        return jsonFilePath;
    }

    public void setJsonFilePath(String jsonFilePath) {
        this.jsonFilePath = jsonFilePath;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Boolean getHasAep() {
        return hasAep;
    }

    public void setHasAep(Boolean hasAep) {
        this.hasAep = hasAep;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public long getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(long lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }
}
