package com.lottie.search.entity;

import javax.persistence.*;

@Entity
@Table(name="animation")
public class Animation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="title")
    private String title;

    @Column(name = "json_file_path")
    private String jsonFilePath;

    @Column(name = "color")
    private String color;

    @Column(name = "description")
    private String description;

    @Column(name= "preview_url")
    private String previewUrl;

    @Column(name = "status")
    private int status;

    @Column(name = "style")
    private int style;

    @Column(name = "type")
    private int type;

    @Column(name = "author_id")
    private int authorId;

    @Column(name = "has_aep")
    private Boolean hasAep;

    @Column(name = "download_count")
    private int downloadCount;

    @Column(name = "last_updated_at")
    private long lastUpdatedAt;

    public Animation() {

    }

    public Animation(String title, String jsonFilePath, String color, String description, String previewUrl, int status, int style, int type, int authorId, Boolean hasAep, int downloadCount, long lastUpdatedAt) {
        this.title = title;
        this.jsonFilePath = jsonFilePath;
        this.color = color;
        this.description = description;
        this.previewUrl = previewUrl;
        this.status = status;
        this.style = style;
        this.type = type;
        this.authorId = authorId;
        this.hasAep = hasAep;
        this.downloadCount = downloadCount;
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String fileName) {
        this.title = fileName;
    }

    public String getJsonFilePath() {
        return jsonFilePath;
    }

    public void setJsonFilePath(String jsonFilePath) {
        this.jsonFilePath = jsonFilePath;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public Boolean getHasAep() {
        return hasAep;
    }

    public void setHasAep(Boolean hasAep) {
        this.hasAep = hasAep;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public long getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(long lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    @Override
    public String toString() {
        return "Animation{" +
                "id=" + id +
                ", fileName='" + title + '\'' +
                ", jsonFilePath='" + jsonFilePath + '\'' +
                ", color='" + color + '\'' +
                ", description='" + description + '\'' +
                ", previewUrl='" + previewUrl + '\'' +
                ", status=" + status +
                ", style=" + style +
                ", type=" + type +
                ", authorId=" + authorId +
                ", hasAep=" + hasAep +
                ", downloadCount=" + downloadCount +
                ", lastUpdatedAt=" + lastUpdatedAt +
                '}';
    }
}
