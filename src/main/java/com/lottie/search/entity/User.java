package com.lottie.search.entity;

import javax.persistence.*;

@Entity
@Table(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name = "animator_bio")
    private String animatorBio;

    @Column(name="animator_location")
    private String animatorLocation;

    @Column(name="animator_url")
    private String animatorUrl;

    @Column(name="avatar")
    private String avatar;

    @Column(name="email")
    private String email;

    @Column(name="entries_count")
    private int entriesCount;

    @Column(name="instagram")
    private String instagram;

    @Column(name="behance")
    private String behance;

    @Column(name="is_hire")
    private int isHire;

    @Column(name="name")
    private String name;

    public User() {
    }

    public User(String animatorBio, String animatorLocation, String animatorUrl, String avatar, String email,
                int entriesCount, String instagram, String behance, int isHire, String name) {
        this.animatorBio = animatorBio;
        this.animatorLocation = animatorLocation;
        this.animatorUrl = animatorUrl;
        this.avatar = avatar;
        this.email = email;
        this.entriesCount = entriesCount;
        this.instagram = instagram;
        this.behance = behance;
        this.isHire = isHire;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnimatorBio() {
        return animatorBio;
    }

    public void setAnimatorBio(String animatorBio) {
        this.animatorBio = animatorBio;
    }

    public String getAnimatorLocation() {
        return animatorLocation;
    }

    public void setAnimatorLocation(String animatorLocation) {
        this.animatorLocation = animatorLocation;
    }

    public String getAnimatorUrl() {
        return animatorUrl;
    }

    public void setAnimatorUrl(String animatorUrl) {
        this.animatorUrl = animatorUrl;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEntriesCount() {
        return entriesCount;
    }

    public void setEntriesCount(int entriesCount) {
        this.entriesCount = entriesCount;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getBehance() {
        return behance;
    }

    public void setBehance(String behance) {
        this.behance = behance;
    }

    public int getHire() {
        return isHire;
    }

    public void setHire(int hire) {
        isHire = hire;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", animatorBio='" + animatorBio + '\'' +
                ", animatorLocation='" + animatorLocation + '\'' +
                ", animatorUrl='" + animatorUrl + '\'' +
                ", avatar='" + avatar + '\'' +
                ", email='" + email + '\'' +
                ", entriesCount=" + entriesCount +
                ", instagram='" + instagram + '\'' +
                ", behance='" + behance + '\'' +
                ", isHire=" + isHire +
                ", name='" + name + '\'' +
                '}';
    }
}
