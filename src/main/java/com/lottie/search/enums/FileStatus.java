package com.lottie.search.enums;

public enum FileStatus {
    PUBLIC(0),
    PRIVATE(1);

    private int value;

    private FileStatus(int value){
        this.value = value;
    }


}
