package com.lottie.search.enums;

public enum FileStyleEnum {
    OUTLINE(0),
    ICON(1),
    ILLUSTRATION(2),
    EMOJI(3),
    STICKER(4);


    private int value;

    private FileStyleEnum(int value){
        this.value = value;
    }


}
