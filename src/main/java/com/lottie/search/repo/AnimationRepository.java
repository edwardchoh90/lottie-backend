package com.lottie.search.repo;

import com.lottie.search.dto.response.AnimationSearchResponse;
import com.lottie.search.entity.Animation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface AnimationRepository extends JpaRepository<Animation, Integer> {


    Page<Animation> findAll(Specification<Animation> spec, Pageable pageable);

    @Query("select new com.lottie.search.dto.response.AnimationSearchResponse(" +
            "usr.animatorUrl," +
            "usr.avatar," +
            "usr.email," +
            "usr.name," +
            "ani.title," +
            "ani.jsonFilePath," +
            "ani.color," +
            "ani.description," +
            "ani.previewUrl," +
            "ani.status," +
            "ani.style," +
            "ani.type," +
            "ani.hasAep," +
            "ani.downloadCount," +
            "ani.lastUpdatedAt) from Animation ani join User usr on ani.authorId = usr.id" +
            " where (ani.title like concat('%' , :text , '%') or usr.name like concat('%' , :text , '%')) and ani.type in :type " +
            "and ani.color like concat('%' , :color , '%') and ani.status in :category and ani.style in :style")
    Page<AnimationSearchResponse> findAllWithAuthors(String text, int[] type, int[] category, String color, int[] style, Pageable pageable);
}
