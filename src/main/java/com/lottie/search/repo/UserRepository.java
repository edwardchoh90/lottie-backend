package com.lottie.search.repo;

import com.lottie.search.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    Page<User> findAll(Specification<User> spec, Pageable pageable);

    User findUsersById(int id);
}
