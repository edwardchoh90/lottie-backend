package com.lottie.search.service;

import com.lottie.search.dto.response.AnimationSearchResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AnimationService {

    public Page<AnimationSearchResponse> findAll(String text, int[] type, int[] category, String color, int[] style, Pageable pageable);


}
