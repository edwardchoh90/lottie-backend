package com.lottie.search.service;

import com.lottie.search.entity.Animation;
import com.lottie.search.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface UserService {

    public Page<User> findAll(Specification<User> specification, Pageable pageable);


}
