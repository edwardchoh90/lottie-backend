package com.lottie.search.service.impl;

import com.lottie.search.dto.response.AnimationSearchResponse;
import com.lottie.search.repo.AnimationRepository;
import com.lottie.search.service.AnimationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AnimationServiceImpl implements AnimationService {

    private AnimationRepository animationRepository;


    public AnimationServiceImpl(AnimationRepository theAnimationRepository){
        animationRepository = theAnimationRepository;
    }


    @Override
    public Page<AnimationSearchResponse> findAll(String text, int[] type, int[] category, String color, int[] style, Pageable pageable) {
        return this.animationRepository.findAllWithAuthors(text, type,category,color,style, pageable);
    }
}
