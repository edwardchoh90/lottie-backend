package com.lottie.search.service.impl;

import com.lottie.search.entity.Animation;
import com.lottie.search.entity.User;
import com.lottie.search.repo.AnimationRepository;
import com.lottie.search.repo.UserRepository;
import com.lottie.search.service.AnimationService;
import com.lottie.search.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;


    public UserServiceImpl(UserRepository theUserRepository){
        userRepository = theUserRepository;
    }


    @Override
    public Page<User> findAll(Specification<User> specification, Pageable pageable) {
        return userRepository.findAll(specification, pageable);
    }
}
